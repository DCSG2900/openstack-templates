#!/bin/bash
ZONE=dns_zone
IP=$(hostname -I | cut -d ' ' -f1)

sudo apt-get update
sudo apt-get install bind9 -y

echo "zone \"${ZONE}\" IN { // Domain name

     type master; // Primary DNS

     file \"/etc/bind/${ZONE}.db\"; // Forward lookup file

     allow-update { none; }; // Since this is the primary DNS, it should be none.

};" | sudo tee /etc/bind/named.conf.local

echo ";
; BIND data file for local loopback interface
;
\$TTL    604800
@       IN      SOA     manager.${ZONE}. root.manager.${ZONE}. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@       IN      NS      manager.${ZONE}.
manager IN      A       ${IP}" | sudo tee /etc/bind/${ZONE}.db

sudo systemctl restart bind9
sudo systemctl enable bind9
