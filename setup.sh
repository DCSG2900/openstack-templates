#!/bin/bash
RKE_DOWNLOAD="https://github.com/rancher/rke/releases/download/v1.1.17-rc2/rke_linux-amd64"


## Create keys if they dont already exists
[ ! -d keys ] && mkdir keys
[ ! -f keys/management ] && [ ! -f keys/management.pub ] && ssh-keygen -f keys/management

i=0

#Deploy stack
CREATE_STACK=TRUE
openstack stack show dcsg2900
if [[ $? == 0 ]]; then
  read -r -p "Stack DCSG2900 already exists, do you want to delete it? [y/N] " response
  if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
  then
      openstack stack delete dcsg2900
      CREATE_STACK=TRUE
      until [ $i -gt 5 ]
      do
        /usr/bin/openstack stack show dcsg2900
        if [[ $? == 0 ]]; then
          sleep 5
        else
          break;
        fi
        ((i=i+1))
      done
    else
      CREATE_STACK=FALSE
    fi
fi
i=0

if [[ $CREATE_STACK == TRUE ]]; then
  openstack stack create dcsg2900 -t rancher2.yaml -e config.environment
  [[ $? != 0 ]] && exit 2 #Failed to create stack
fi
#Wait til stack is deployed
status='FAILED'
until [ $i -gt 12 ]
do
  status=$(/usr/bin/openstack stack show dcsg2900 -c 'stack_status' -f value )
  [[ $status == *_COMPLETE ]] &&  break
  sleep 10
  ((i=i+1))
done

#Exit script if stack deploy failed
[[ $status != *_COMPLETE ]] && exit 1

#Get manager IP
MANAGER_PUBLIC_IP=$(openstack stack output show dcsg2900 manager_ip -f value -c output_value)

##Wait til rancher comes up
##Wait for rancher to come up
echo -n "Waiting for rancher to come up."
while : ; do
  RESPONSE="$(curl -sk --header 'Host: manager.dcsg2900' 'https://'$MANAGER_PUBLIC_IP':443/ping')"
  STATUSCODE=$?
  RANCHER_AVAILABLE=FALSE
  if [ $STATUSCODE -eq 0 ]; then
    if [ "$RESPONSE" == 'pong' ]; then
      RANCHER_AVAILABLE=TRUE
    fi
  fi
  [ $RANCHER_AVAILABLE == TRUE ] && break;
  echo -n '.'
  sleep 3
done

#Create ha_cluster.yml
./createcluster.sh

echo "Rancher is now setup, IP of manager is: $MANAGER_PUBLIC_IP, please do the following manual steps to finnish setup"
echo "1.  Copy the ha_cluster.yml file to ubuntu@$MANAGER_PUBLIC_IP"
echo "1.a For example 'scp ha_cluster.yml ubuntu@$MANAGER_PUBLIC_IP:ha_cluster.yml'"
echo "2.  SSH into ubuntu@$MANAGER_PUBLIC_IP and run './rke up --config ha_cluster.yml' to setup the HA rke cluster."
echo "3.  Once the HA rke cluster is setup, run 'kubectl --kubeconfig kube_config_ha_cluster.yml apply -f /RANCHER_dcsg2900ha.yaml' to import the HA cluster into rancher"
echo "4.  Logout of ubuntu@$MANAGER_PUBLIC_IP, you can now access your rancher instance by going to manager.dcsg2900 at the ip $MANAGER_PUBLIC_IP"
echo "4.a An easy way to do this is to add '$MANAGER_PUBLIC_IP manager.dcsg2900' to your host file (/etc/hosts)"
echo "5.  If you want to add an low latency hardware cluster like described in the thesis, do this through the rancher dashboard"