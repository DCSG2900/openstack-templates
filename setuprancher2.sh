#!/bin/bash

TLD=environment_domain
CAPASS=certificate_passphrase
RANCHER_PASSWORD=rancher_admin_password
DEFAULT_PASS=rancher_default_password
MANAGER_URL="manager.$TLD"
RKE_DOWNLOAD="https://github.com/rancher/rke/releases/download/v1.1.17-rc2/rke_linux-amd64"
CLUSTER_NAME="dcsg2900ha"

#Ensure running latest packages
sudo apt-get update
#sudo apt-get upgrade -y
MANAGERIPPRIV=$(hostname -I | cut -d ' ' -f1)
#Install K3S
curl https://get.k3s.io | INSTALL_K3S_VERSION=v1.19.7+k3s1 sh
mkdir ~/.kube
sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/config

#Installing helm
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm -y

#Instlaling rancher
### CERTS

LOCATIOn=$PWD

###Get random
dd if=/dev/urandom of=$LOCATION/.rnd bs=1M count=1
##CA private key
openssl genrsa -passout pass:'${CAPASS}' -des3 -out cakeys.pem 2048
##CA root cert
openssl req -passin pass:'${CAPASS}' -rand $LOCATION/.rnd -new -key cakeys.pem -x509 -days 1000 -out cacerts.pem -subj "/C=NO/ST=Oppland/L=Gjovik/O=NTNU DCSG2900/OU=G118/CN=certificate-authority.${TLD}"
rm $LOCATION/.rnd
dd if=/dev/urandom of=$LOCATION/.rnd bs=1M count=1
##Server private key
openssl req -new -nodes -rand $LOCATION/.rnd -newkey rsa:4096 -keyout manager.key -out manager.req -batch -subj "/C=NO/ST=Oppland/L=Gjovik/O=NTNU DCSG2900/OU=G118/CN=manager.${TLD}" -reqexts SAN -config <(cat /etc/ssl/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:manager-${TLD},IP:${MANAGERIPPRIV}"))
openssl x509 -req -in manager.req -CA cacerts.pem -CAkey cakeys.pem -passin pass:'${CAPASS}' -CAcreateserial -out manager.crt -days 3650 -sha256 -extfile <(printf "subjectAltName=DNS:manager.${TLD},IP:${MANAGERIPPRIV}")
rm $LOCATION/.rnd

##Make one full chain
cat manager.crt cacerts.pem > tls.crt

## Adding stable helm repo
sudo helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
sudo helm repo update
##Create a namespace for rancher
sudo kubectl create namespace cattle-system

##Install with own cert
sudo helm install rancher rancher-stable/rancher \
  --namespace cattle-system \
  --set hostname=manager.${TLD} \
  --set ingress.tls.source=secret \
  --set privateCA=true

sudo kubectl -n cattle-system create secret tls tls-rancher-ingress \
  --cert=tls.crt \
  --key=manager.key

sudo kubectl -n cattle-system create secret generic tls-ca   --from-file=cacerts.pem=./cacerts.pem

sudo chown -R ubuntu:ubuntu /home/ubuntu


## THE REST OF THE SCRIPT SETUPS RANCHER BY TALKING TO ITS API
## THIS IS A SLIGHT EDIT OF A PUBLIC SCRIPT FOUND HERE: https://github.com/ozbillwang/rancher-in-kind/blob/master/add-cluster.sh


#Check if JQ is installed, if not install it
jq
if [[ $? == 127 ]]; then
  sudo apt-get install -y jq
fi

##Update host file, add manager url
sudo sed -i -r "s/127.0.0.1 localhost\b/127.0.0.1 $MANAGER_URL/g" /etc/hosts

##Wait for rancher to come up

while : ; do
  RESPONSE="$(curl -sk 'https://'$MANAGER_URL':443/ping')"
  STATUSCODE=$?
  RANCHER_AVAILABLE=FALSE
  if [ $STATUSCODE -eq 0 ]; then
    if [ $RESPONSE == 'pong' ]; then
      RANCHER_AVAILABLE=TRUE
    fi
  fi
  [ $RANCHER_AVAILABLE == TRUE ] && break;
  echo "Waiting for rancher to be available"
  sleep 3
done


function login() {
  LOGINRESPONSE=`curl -s "https://${MANAGER_URL}/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary "{\"username\":\"admin\",\"password\":\"$1\"}" --insecure`
  LOGINTOKEN=`echo $LOGINRESPONSE | jq -r .token`
}

#Login With default password
login $DEFAULT_PASS

##Change password to one requested
curl -s "https://${MANAGER_URL}/v3/users?action=changepassword" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --data-binary '{"currentPassword":"'${DEFAULT_PASS}'","newPassword":"'${RANCHER_PASSWORD}'"}' --insecure

#Login with new password
login $RANCHER_PASSWORD

# Create API key
APITOKEN=`curl -s "https://${MANAGER_URL}/v3/token" -H 'content-type: application/json' -H "Authorization: Bearer $LOGINTOKEN" --data-binary '{"type":"token","description":"automation"}' --insecure | jq -r .token`

#Configure server-url
curl -s "https://${MANAGER_URL}/v3/settings/server-url" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" -X PUT --data-binary '{"name":"server-URL","value":"https://'$MANAGER_URL'"}' --insecure


#Create cluster
CLUSTERID=`curl -s "https://${MANAGER_URL}/v3/cluster" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary '{"type":"cluster","name":"'${CLUSTER_NAME}'","import":true}' --insecure | jq -r .id`
MANIFEST=`curl -s "https://${MANAGER_URL}/v3/clusters/${CLUSTERID}/clusterregistrationtoken" -H 'content-type: application/json' -H "Authorization: Bearer $APITOKEN" --data-binary '{"type":"clusterRegistrationToken","clusterId":"'$CLUSTERID'"}' --insecure | jq -r .manifestUrl`
#Download manifest
MANIFEST_FILE="RANCHER_$CLUSTER_NAME.yaml"
curl -ko $MANIFEST_FILE $MANIFEST

#Download RKE
wget -O /home/ubuntu/rke $RKE_DOWNLOAD
chown ubuntu:ubuntu /home/ubuntu/rke
chmod +x /home/ubuntu/rke