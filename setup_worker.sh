#!/bin/bash
curl https://releases.rancher.com/install-docker/19.03.14.sh | sh
sudo usermod -aG docker ubuntu
echo "MANAGEMENT_PUB" | sudo tee -a /home/ubuntu/.ssh/authorized_keys
sudo chown -R ubuntu:ubuntu /home/ubuntu/.ssh/authorized_keys

echo "network: {configs: disabled}" | sudo tee /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg
sudo sed -i 's/nameserver .*/nameserver RANCHER_IP/' /etc/resolv.conf
