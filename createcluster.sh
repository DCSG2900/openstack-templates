#!/bin/bash

#Setup ha_cluster.yml file
function add_nodes_to_cluster() {
  #Get outputs from openstack
  ids=$(openstack stack output show dcsg2900 "$1_ids" -c output_value -f json)
  names=$(openstack stack output show dcsg2900 "$1_names" -c output_value -f json)
  private_ips=$(openstack stack output show dcsg2900 "$1_private_ips" -c output_value -f json)
  public_ips=$(openstack stack output show dcsg2900 "$1_public_ips" -c output_value -f json)

  #Get count of nodes
  count=$(jq '.output_value | length' <<< "$ids")
  #Loop over all nodes and add them to ha_cluster.yml
  for (( i=0; i < ${count}; i++ )); do
    #Get name, private and public ip for the node
    name=$(jq -r ".output_value[$i]" <<< "$names")
    private_ip=$(jq -r ".output_value[$i]" <<< "$private_ips")
    public_ip=$(jq -r ".output_value[$i]" <<< "$public_ips")

#Add to file
cat <<EOT >> ha_cluster.yml
  - hostname_override: $name
    address: $public_ip
    internal_address: $private_ip
    role:
      - $1
    user: ubuntu
    ssh_key_path: /home/ubuntu/.ssh/management
EOT
  done
}

if [ -f ha_cluster.yml ]; then
  read -p "File ha_cluster.yml exists, should it be regenerated? (Y/n)" -n 1 -r should_gen_ha_cluster_response
  echo #new line
else
  should_gen_ha_cluster_response='y'
fi

if [[ "$should_gen_ha_cluster_response" =~ ^[yY]$ ]]; then
  echo "nodes:" > ha_cluster.yml
  add_nodes_to_cluster "etcd"
  add_nodes_to_cluster "controlplane"
  add_nodes_to_cluster "worker"
  #Traefik mesh has problems with the default coredns version 1.8.0, as it fails to recognize it as version 1.8
  echo "system_images:" >> ha_cluster.yml
  echo "  coredns: rancher/coredns-coredns:1.7.1" >> ha_cluster.yml
fi